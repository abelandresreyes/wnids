#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 17:56:13 2019

@author: root
"""
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import numpy as np
import itertools
import matplotlib.pyplot as plt
from scipy.stats import mode
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from numpy import array
from sklearn.ensemble import BaggingClassifier
from sklearn.model_selection import GridSearchCV
import time
#este es para hacer con el enfoque de agrupar injection and impersonation
######99.25% de accuracy es el resultado de agrupar injection and impersonation
#Hacer otro scrip que no tenga entrenamiento especifico para las 3 clases al inicio, que solo
#clasifique y luego de la clasificacion separamos y calculamos el accuracy
################################################DATA LOADING########################################################################
#actual dataframe
#training dataframe
srcTrn = 'datasets/Trn_NZVAR_37.csv'
dfTrn = pd.read_csv(srcTrn, header=None, low_memory= False)
dfTrn_2 = pd.read_csv(srcTrn, header=None, low_memory= False)
#test dataframe
srcTst = 'datasets/Tst_NZVAR_37.csv'
dfTst = pd.read_csv(srcTst, header=None, low_memory= False)
dfTst_2 = pd.read_csv(srcTst, header=None, low_memory= False)

import seaborn as sns
#get correlations of each features in dataset
#corrmat = dfTrn.corr()
#top_corr_features = corrmat.index
#plt.figure(figsize=(36,36))
##plot heat map
#g=sns.heatmap(dfTrn[top_corr_features].corr(),annot=True,cmap="RdYlGn")


#corr = dfTrn.corr()
#ax = sns.heatmap(
#    corr,
#    vmin=-1, vmax=1, center=0,
#    cmap=sns.diverging_palette(20, 220, n=200),
#    square=True
#)
#ax.set_xticklabels(
#    ax.get_xticklabels(),
#    rotation=45,
#    horizontalalignment='right'
#);

        #dataframe for the first model
dfTrn1=pd.DataFrame(dfTrn)
dfTst1=pd.DataFrame(dfTst)
dfTrn2=pd.DataFrame(dfTrn_2)
dfTst2=pd.DataFrame(dfTst_2)
y_actualname=dfTrn.iloc[:,-1]

################################################DATA PREPROCESSING#############################################################

#######################Feature selection - Correlation analysis based###################################
# 0, 3 epoch time
# 1, 2
# 4, 5
# 6, 7, 8, 9, 10, 11, 12
# 13 mac time
# 16 and 17 are negatively correlated
# Attributes with missing values: 15, 16, 18, 28, 29-33, 34, 35
# Att20+ Att21 = Att19

values_to_replace = {'0x00': 0, '0x01': 1, '0x02': 2}
dfTrn1[22] = dfTrn[22].map(values_to_replace)
dfTst1[22] = dfTst[22].map(values_to_replace)
dfTrn2[22] = dfTrn[22].map(values_to_replace)
dfTst2[22] = dfTst[22].map(values_to_replace)

#actual name of the labels
values_to_replace1 = {'normal':3, 'injection':2, 'flooding': 0, 'impersonation':1}
#values_to_replace1 = {'normal':0, 'injection':1, 'flooding': 2, 'impersonation':3}
#values_to_replace1 = {'normal':0, 'injection':1, 'flooding': 2, 'impersonation':3}
values_to_replace2 = {'normal':3, 'injection':2, 'flooding': 0, 'impersonation':1}
dfTrn1[36] = dfTrn[36].map(values_to_replace1)
dfTst1[36] = dfTst[36].map(values_to_replace1)
dfTrn2[36] = dfTrn2[36].map(values_to_replace2)
dfTst2[36] = dfTst2[36].map(values_to_replace2)

ind = dfTrn1[15].isna()
dfTrn1.at[ind, 15] = 2437.0
ind = dfTst1[15].isna()
dfTst1.at[ind, 15] = 2437.0

ind = dfTrn1[16].isna()
dfTrn1.at[ind, 16] = 0.0
ind = dfTst1[16].isna()
dfTst1.at[ind, 16] = 0.0

ind = dfTrn1[18].isna()
dfTrn1.at[ind, 18] = -21.0
ind = dfTst1[18].isna()
dfTst1.at[ind, 18] = -21.0

ind = dfTrn1[28].isna()
dfTrn1.at[ind, 28] = 44.0
ind = dfTst1[28].isna()
dfTst1.at[ind, 28] = 44.0

ind = dfTrn1[29].notna()
dfTrn1.at[ind, 29] = 1
ind = dfTrn1[29].isna()
dfTrn1.at[ind, 29] = 0

ind = dfTst1[29].notna()
dfTst1.at[ind, 29] = 1
ind = dfTst1[29].isna()
dfTst1.at[ind, 29] = 0

ind = dfTrn1[30].notna()
dfTrn1.at[ind, 30] = 1
ind = dfTrn1[30].isna()
dfTrn1.at[ind, 30] = 0

ind = dfTst1[30].notna()
dfTst1.at[ind, 30] = 1
ind = dfTst1[30].isna()
dfTst1.at[ind, 30] = 0

ind = dfTrn1[31].notna()
dfTrn1.at[ind, 31] = 1
ind = dfTrn1[31].isna()
dfTrn1.at[ind, 31] = 0

ind = dfTst1[31].notna()
dfTst1.at[ind, 31] = 1
ind = dfTst1[31].isna()
dfTst1.at[ind, 31] = 0

ind = dfTrn1[32].notna()
dfTrn1.at[ind, 32] = 1
ind = dfTrn1[32].isna()
dfTrn1.at[ind, 32] = 0

ind = dfTst1[32].notna()
dfTst1.at[ind, 32] = 1
ind = dfTst1[32].isna()
dfTst1.at[ind, 32] = 0

ind = dfTrn1[33].notna()
dfTrn1.at[ind, 33] = 1
ind = dfTrn1[33].isna()
dfTrn1.at[ind, 33] = 0

ind = dfTst1[33].notna()
dfTst1.at[ind, 33] = 1
ind = dfTst1[33].isna()
dfTst1.at[ind, 33] = 0

ind = dfTrn1[34].isna()
dfTrn1.at[ind, 34] = 0
ind = dfTst1[34].isna()
dfTst1.at[ind, 34] = 0

##############################################dftst and dftrn2
ind = dfTrn2[15].isna()
dfTrn2.at[ind, 15] = 2437.0
ind = dfTst2[15].isna()
dfTst2.at[ind, 15] = 2437.0

ind = dfTrn2[16].isna()
dfTrn2.at[ind, 16] = 0.0
ind = dfTst2[16].isna()
dfTst2.at[ind, 16] = 0.0

ind = dfTrn2[18].isna()
dfTrn2.at[ind, 18] = -21.0
ind = dfTst2[18].isna()
dfTst2.at[ind, 18] = -21.0

ind = dfTrn2[28].isna()
dfTrn2.at[ind, 28] = 44.0
ind = dfTst2[28].isna()
dfTst2.at[ind, 28] = 44.0

ind = dfTrn2[29].notna()
dfTrn2.at[ind, 29] = 1
ind = dfTrn2[29].isna()
dfTrn2.at[ind, 29] = 0

ind = dfTst2[29].notna()
dfTst2.at[ind, 29] = 1
ind = dfTst2[29].isna()
dfTst2.at[ind, 29] = 0

ind = dfTrn2[30].notna()
dfTrn2.at[ind, 30] = 1
ind = dfTrn2[30].isna()
dfTrn2.at[ind, 30] = 0

ind = dfTst2[30].notna()
dfTst2.at[ind, 30] = 1
ind = dfTst2[30].isna()
dfTst2.at[ind, 30] = 0

ind = dfTrn2[31].notna()
dfTrn2.at[ind, 31] = 1
ind = dfTrn2[31].isna()
dfTrn2.at[ind, 31] = 0

ind = dfTst2[31].notna()
dfTst2.at[ind, 31] = 1
ind = dfTst2[31].isna()
dfTst2.at[ind, 31] = 0

ind = dfTrn2[32].notna()
dfTrn2.at[ind, 32] = 1
ind = dfTrn2[32].isna()
dfTrn2.at[ind, 32] = 0

ind = dfTst2[32].notna()
dfTst2.at[ind, 32] = 1
ind = dfTst2[32].isna()
dfTst2.at[ind, 32] = 0

ind = dfTrn2[33].notna()
dfTrn2.at[ind, 33] = 1
ind = dfTrn2[33].isna()
dfTrn2.at[ind, 33] = 0

ind = dfTst2[33].notna()
dfTst2.at[ind, 33] = 1
ind = dfTst2[33].isna()
dfTst2.at[ind, 33] = 0

ind = dfTrn2[34].isna()
dfTrn2.at[ind, 34] = 0
ind = dfTst2[34].isna()
dfTst2.at[ind, 34] = 0

ind = dfTrn1[35].isna()
dfTrn1.at[ind, 35] = 1703.0
ind = dfTst1[35].isna()
dfTst1.at[ind, 35] = 1703.0


ind = dfTrn1[17].isna()
dfTrn1.at[ind, 17] = 1.0
ind = dfTst1[17].isna()
dfTst1.at[ind, 17] = 1.0

corr = dfTrn1.corr()
ax = sns.heatmap(
    corr,
    vmin=-1, vmax=1, center=0,
    cmap=sns.diverging_palette(20, 220, n=200),
    square=True
)
ax.set_xticklabels(
    ax.get_xticklabels(),
    rotation=45,
    horizontalalignment='right'
);


########################Features selected after FEATURE IMPORTANCE ANALYSIS############################
ftrs_lst = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34,35]#############PARA EL PRIMER MODELO %%%99.415 accuracy
#ftrs_lst = [4, 8, 14, 16, 18, 22, 24, 25, 26, 27, 28, 29, 30, 34,35]#############PYSWARM feature selection
#n = 36
#exc = [13, 19]
#exc=[2,5, 11, 12, 13, 14, 19, 20, 21, 33]
#ftrs_lst= [i for i in range(n) if i not in exc]#PYSWARM feature selection
#[1 1 0 1 1 0 1 1 1 1 1 0 0 0 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 1 0 1 1]

#ftrs_lst = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34,35]####96.19%
####0,5,13,8,11,14
#ftrs_lst2 = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34]#############PARA EL PRIMER MODELO
#ftrs_lst2 = [4, 18, 22, 25, 27,28]
#ftrs_function=[0,3,6,7,8,9]
#ftrs_function=[0,5,8,11,13,14] ####This goes to the function, store the positions in the array of features (the one used for the first model)

#ftrs_lst2 = [4, 18, 21, 22, 27,28]
#ftrs_function=[0,5,7,8,13,14]
ftrs_lst2 = [4, 18, 21, 22, 27,28,35]
ftrs_function=[0,5,7,8,13,14,18]
#ftrs_function=[4,17,19,20,25,26]

##Features for the first model prediction - INPUTS for the first MODEL  (training)
X = dfTrn1.iloc[:, ftrs_lst].values
##Features for the first model prediction - Outputs for the first MODEL (training)
Y = dfTrn1.iloc[:, 36].values
X_attack_training=dfTrn2.iloc[:, ftrs_lst2].values
X_attack_test=dfTst2.iloc[:, ftrs_lst2].values

Y_attack=dfTrn2.iloc[:, 36].values
##Features for the first model prediction - INPUTS for the first MODEL  (test)
X_tst = dfTst1.iloc[:, ftrs_lst].values
##Features for the first model prediction - Outputs for the first MODEL (test)
Y_tst = dfTst1.iloc[:, 36].values
Y_tst_attack=dfTst2.iloc[:, 36].values





##splitting the data in training and test sets
##Converting the data from array to dataframe to keep track of them
#########ORIGINAL DATA FOR REFERENCES########################
X_train_df=pd.DataFrame(X)
X_test_df=pd.DataFrame(X_tst)
y_train_df=pd.DataFrame(Y)
y_test_df=pd.DataFrame(Y_tst)

###################################for the second model
X_attack_train_df=pd.DataFrame(X_attack_training)
X_attack_test_df=pd.DataFrame(X_attack_test)
y_attack_train_df=pd.DataFrame(Y_attack)
y_attack_test_df=pd.DataFrame(Y_tst_attack)



############DATA FRAME for output USING FOR references after the first classifier
y_train_df1=pd.DataFrame()
y_train_df1=y_train_df1.append(y_train_df)

y_test_df1=pd.DataFrame()
y_test_df1=y_test_df1.append(y_test_df)



############gridsearch######### best random_state=203 or 333
#rs=list(range(1000))
#gs=GridSearchCV(RandomForestClassifier(), {'random_state': rs}, n_jobs=-1, cv=5, scoring="accuracy")
#gs.fit(X_train_df,y_train_df)
#print(gs.best_params_)
#print(gs.best_score_)
#model1=RandomForestClassifier().set_params(**gs.best_params_)
#model1 = RandomForestClassifier(n_estimators= 320, max_depth=12, random_state = 333)
#model1 = RandomForestClassifier(random_state = 2)
#model1=RandomForestClassifier(bootstrap= True, max_depth= 80, max_features= 2, min_samples_leaf= 5, min_samples_split= 12, n_estimators= 100, random_state= 500)
#model1 = RandomForestClassifier(random_state = 107) ###96.2155%
start_1MLmodel_training=time.time()
model1 = RandomForestClassifier(random_state = 203)
#model1 = RandomForestClassifier(n_estimators =10, min_samples_leaf = 1, min_samples_split = 2, random_state = 203)

#model1 = RandomForestClassifier(n_estimators= 320, max_depth=12, random_state = 203)
model1.fit(X_train_df,y_train_df)
end_1MLmodel_training = time.time()
print('Time elapsed for training first ML model (RF): %.4f'%(end_1MLmodel_training-start_1MLmodel_training))
#print('Time elapsed_RF: %.4f'%(end-start))
#scores = cross_val_score(model1, X_train_df,y_train_df, cv=10, scoring='accuracy')
#score = scores.mean()*100
#print('Accuracy: %.4f'%(score ))    

start_1MLmodel_predictions=time.time()
predictions1=model1.predict(X_test_df)
end_1MLmodel_predictions = time.time()
print('Time elapsed for prediction with first ML model (RF): %.4f'%(end_1MLmodel_predictions-start_1MLmodel_predictions))
accuracy_model1=accuracy_score(y_test_df,predictions1)
print("The accuray after the first classifier (4 classes)is: ")
print(accuracy_model1)
cm0=confusion_matrix(y_test_df1,predictions1)
cm0

classes4 = ['flooding', 'impersonation', 'injection', 'normal']
fig = plt.figure()
plt.imshow(cm0, interpolation='nearest', cmap=plt.cm.Blues)
plt.colorbar()
tick_marks = np.arange(len(classes4))
plt.xticks(tick_marks, classes4, rotation=45)
plt.yticks(tick_marks, classes4)

fmt = 'd'
np.set_printoptions(precision=2)
thresh = cm0.max() / 2.
for i, j in itertools.product(range(cm0.shape[0]), range(cm0.shape[1])):
    plt.text(j, i, format(cm0[i, j], fmt), horizontalalignment="center", color="white" if cm0[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label', fontweight='bold')
    plt.xlabel('Predicted label', fontweight='bold')
   
plt.show()
fig.savefig('confusion_matrix_0.png')

cr0 = classification_report(y_test_df1, predictions1, target_names=classes4)
print(cr0)
accuracy_0 = accuracy_score(y_test_df1, predictions1)*100
print("The accuray after the first classifier (4 classes)is: ",accuracy_0,"%")
#print(accuracy_0)

####################################mapping values to see how good is with 3 classes
y_test_df1[y_test_df1 == 1] = 2 ##imper/injection
y_test_df1[y_test_df1 == 0] = 1 ##flooding
y_test_df1[y_test_df1 == 3] = 0 ##normal

predictions1[predictions1 == 1] = 2##imper/injection
predictions1[predictions1 == 0] = 1##flooding
predictions1[predictions1 == 3] = 0##normal

cm1=confusion_matrix(y_test_df1,predictions1)
cm1

#classes4 = ['flooding', 'impersonation', 'injection', 'normal']
classes3 = ['normal', 'flooding', 'imper/inject']
fig = plt.figure()
plt.imshow(cm1, interpolation='nearest', cmap=plt.cm.Blues)
plt.colorbar()
tick_marks = np.arange(len(classes3))
plt.xticks(tick_marks, classes3, rotation=45)
plt.yticks(tick_marks, classes3)

fmt = 'd'
np.set_printoptions(precision=2)
thresh = cm1.max() / 2.
for i, j in itertools.product(range(cm1.shape[0]), range(cm1.shape[1])):
    plt.text(j, i, format(cm1[i, j], fmt), horizontalalignment="center", color="white" if cm1[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label', fontweight='bold')
    plt.xlabel('Predicted label', fontweight='bold')
   
plt.show()
fig.savefig('confusion_matrix_1.png')

cr1 = classification_report(y_test_df1, predictions1, target_names=classes3)
print(cr1)
accuracy_1 = accuracy_score(y_test_df1, predictions1)*100
print("The accuray after the first classifier (3 classes)is: ",accuracy_1,"%")
#print(accuracy_1)
####################################Feature Selection for second model######
print(model1.feature_importances_)
fig_feat_imp_2model=plt.figure()
index_Feat_Imp_2model=[4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34,35]
feat_importances = pd.Series(model1.feature_importances_, index=index_Feat_Imp_2model)
feat_importances.nlargest(10).plot(kind='barh',color='b')
plt.title('Feature Importance for 2 ML model')
plt.ylabel('Feature #')
plt.xlabel('Importance factor')
plt.show()
fig_feat_imp_2model.savefig('Feature_Importance_2MLmodel.png')




#corrmat = X_train_df.corr()
#top_corr_features = corrmat.index
#plt.figure(figsize=(18,18))
##plot heat map
#g=sns.heatmap(X_train_df[top_corr_features].corr(),annot=True,cmap="RdYlGn")
#
#
#corr = X_train_df.corr()
#ax = sns.heatmap(
#        corr,
#        vmin=-1, vmax=1, center=0,
#        cmap=sns.diverging_palette(20, 220, n=200),
#        square=True
#    )
#ax.set_xticklabels(
#        ax.get_xticklabels(),
#        rotation=45,
#        horizontalalignment='right'
#    );
#


#################################Creating the data set to  training the second model
df_locations_row=array([])
df_locations_row=np.where((y_train_df1!=3) & (y_train_df1!=0))###looking for positions of injection and impersonation attacks
df_locations_row=df_locations_row[0].tolist()

df2=pd.DataFrame()
##populating the second dataframe
df2=X_train_df.ix[df_locations_row]
X_2=df2.iloc[:,:]

#create Y_2 de tal manera que tenga las ouputs originales de las rows que fueron identificadas despues del modelo1
X_attack=pd.DataFrame()
X_attack=X_attack_train_df.ix[df_locations_row]

y_2=pd.DataFrame()
y_2=y_attack_train_df.ix[df_locations_row]


#################################################FEATURE IMPORTANCE#####################################
print(model1.feature_importances_)
feat_importances = pd.Series(model1.feature_importances_, index=X_test_df.columns)
feat_importances.nlargest(10).plot(kind='barh')
plt.show()


###########################################TRAINING THE SECOND MODEL####################################################
from sklearn.naive_bayes import GaussianNB
#start = time.time()

model2 = GaussianNB()
start_2MLmodel_training = time.time()
model2.fit(X_attack,y_2)
end_2MLmodel_training = time.time()
print('Time elapsed for training second ML model (GNB): %.4f'%(end_2MLmodel_training-start_2MLmodel_training))
#scores = cross_val_score(model2, X_attack, y_2, cv=10, scoring='accuracy')
#score = scores.mean()*100
#
#print('Accuracy_GaussianNB: ',score)
#end = time.time()
#print('Time elapsed_GNB: %.4f'%(end-start))

############################################################Trainining and measured performanace for 2ML model##############3
#from sklearn.ensemble import RandomForestClassifier
#seed_lst = [j*7 for j in range(31)]
#mean_scores = []
#start = time.time()
#for seed in seed_lst:
#    model2_RF = RandomForestClassifier(n_estimators =10, min_samples_leaf = 1, min_samples_split = 2, random_state = seed)
#    scores = cross_val_score(model2_RF, X_attack, y_2, cv=10, scoring='accuracy')
#    score = scores.mean()*100
#    mean_scores.append(score)   
#
#mean_res1 = np.array(mean_scores)
#print('Accuracy_RF: %.4f(+/-%.4f)'%(mean_res1.mean(), mean_res1.std()))
#end = time.time()
#print('Time elapsed_RF: %.4f'%(end-start))
#
#
#
#import xgboost as xgb
#seed_lst = [j*7 for j in range(3)]
#mean_scores = []
#start = time.time()
#for seed in seed_lst:
#    model2_XGB = xgb.XGBClassifier(max_depth=10, n_estimators=3, learning_rate=0.1, seed=seed)
#    scores = cross_val_score(model2_XGB, X_attack, y_2, cv=10, scoring='accuracy')
#    score = scores.mean()*100
#    mean_scores.append(score)   
#
#mean_res1 = np.array(mean_scores)
#print('Accuracy_XGB: %.4f(+/-%.4f)'%(mean_res1.mean(), mean_res1.std()))
#end = time.time()
#print('Time elapsed_XGB: %.4f'%(end-start)) 
#
#
#from sklearn.ensemble import ExtraTreesClassifier
#seed_lst = [j*7 for j in range(31)]
#mean_scores = []
#start = time.time()
#for seed in seed_lst:
#    model2_ET = ExtraTreesClassifier(random_state = seed)
#    scores = cross_val_score(model2_ET, X_attack, y_2, cv=10, scoring='accuracy')
#    score = scores.mean()*100
#    mean_scores.append(score)   
#
#mean_res1 = np.array(mean_scores)
#print('Accuracy_ET: %.4f(+/-%.4f)'%(mean_res1.mean(), mean_res1.std()))
#end = time.time()
#print('Time elapsed_ET: %.4f'%(end-start)) 
#
#
#from sklearn.ensemble import BaggingClassifier
#seed_lst = [j*7 for j in range(31)]
#mean_scores = []
#start = time.time()
#for seed in seed_lst:
#    model2_BG = BaggingClassifier(random_state = seed)
#    scores = cross_val_score(model2_BG, X_attack, y_2, cv=10, scoring='accuracy')
#    score = scores.mean()*100
#    mean_scores.append(score)   
#
#mean_res3 = np.array(mean_scores)
#print('Accuracy_BG: %.4f(+/-%.4f)'%(mean_res3.mean(), mean_res3.std()))
#end = time.time()
#print('Time elapsedBG: %.4f'%(end-start))  




##############################Data for testing model 2#######################################
df_locations_row_test=array([])
#df_locations_row_test=np.where((y_test_df1==1)|(y_test_df1==3))
df_locations_row_test=np.where(y_test_df1==2) #locking for the imper/inject class rows
df_locations_row_test=df_locations_row_test[0].tolist()


df2_test=pd.DataFrame()
##populating the second dataframe
df2_test=X_attack_test_df.ix[df_locations_row_test]

##implementing the second model

X_2_test=df2_test.iloc[:,:]
#create Y_2 de tal manera que tenga las ouputs originales de las rows que fueron identificadas despues del modelo1
y_2_test=pd.DataFrame()
y_2_test=y_attack_test_df.ix[df_locations_row_test]
########################################################################################################################
#############################################Testting the second Model##################################################
#model2_RF.fit(X_attack, y_2)
#predictions2_RF=model2_RF.predict(X_2_test)
#accuracy_3_RF = accuracy_score(y_2_test, predictions2_RF)*100
#print("Accuracy of the second model, using Gaussian_RF",accuracy_3_RF)
#
#
#model2_BG.fit(X_attack, y_2)
#predictions2_BG=model2_BG.predict(X_2_test)
#accuracy_3_BG = accuracy_score(y_2_test, predictions2_BG)*100
#print("Accuracy of the second model, using Gaussian_BG",accuracy_3_BG)
#
#model2_ET.fit(X_attack, y_2)
#predictions2_ET=model2_ET.predict(X_2_test)
#accuracy_3_ET = accuracy_score(y_2_test, predictions2_ET)*100
#print("Accuracy of the second model, using Gaussian_ET",accuracy_3_ET)
#
#model2_XGB.fit(X_attack, y_2)
#predictions2_XGB=model2_XGB.predict(X_2_test)
#accuracy_3_XGB = accuracy_score(y_2_test, predictions2_XGB)*100
#print("Accuracy of the second model, using Gaussian_XGB",accuracy_3_XGB)
#






start_2MLmodel_predictions = time.time()
predictions2=model2.predict(X_2_test)
end_2MLmodel_predictions = time.time()
print('Time elapsed for prediction with second ML model (GNB): %.4f'%(end_2MLmodel_predictions-start_2MLmodel_predictions))
accuracy_3 = accuracy_score(y_2_test, predictions2)*100
print("Accuracy of the second model, using Gaussian_NB",accuracy_3,"%")


#print(model2.feature_importances_)
#feat_importances = pd.Series(model2.feature_importances_, index=X_test_df.columns)
#feat_importances.nlargest(10).plot(kind='barh')
#plt.show()



cm2 = confusion_matrix(y_2_test, predictions2)
cm2

#classes3 = ['injection', 'flooding ','impersonation']
classes2 = ['impersonation', 'injection']
fig = plt.figure()
plt.imshow(cm2, interpolation='nearest', cmap=plt.cm.Blues)
plt.colorbar()
tick_marks = np.arange(len(classes2))
plt.xticks(tick_marks, classes2, rotation=45)
plt.yticks(tick_marks, classes2)

fmt = 'd'
np.set_printoptions(precision=2)
thresh = cm2.max() / 2.
for i, j in itertools.product(range(cm2.shape[0]), range(cm2.shape[1])):
    plt.text(j, i, format(cm2[i, j], fmt), horizontalalignment="center", color="white" if cm2[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label', fontweight='bold')
    plt.xlabel('Predicted label', fontweight='bold')
   
plt.show()
fig.savefig('confusion_matrix_2.png')

cr2 = classification_report(y_2_test, predictions2, target_names=classes2)
print(cr2)

#####################################FOR TESTING PURPOSES
#y_predictions_final=pd.DataFrame()
#y_predictions_final=np.where(predictions1==0,3,1)##########################################revisar esta parte
##prediction_final=array([])
#k1=0
#for m in df_locations_row_test:
#    y_predictions_final[m]=predictions2[k1]
#    k1=k1+1
#
#accuracy_final = accuracy_score(y_attack_test_df, y_predictions_final)*100
#cm4 = confusion_matrix(y_attack_test_df,y_predictions_final)
#print(accuracy_final)
#cm4

###########################FUNCTIONS definitions######################
def first_model(data):
    y_predictions_1=pd.DataFrame()####output from the first model
    x_data_1=pd.DataFrame(data)###input for the first model
    y_predictions_1=model1.predict(x_data_1)
    return y_predictions_1

def second_model(data):
    y_predictions_1=pd.DataFrame()####output from the first model
    x_data_1=pd.DataFrame(data)###input for the first model
    y_predictions_1=model2.predict(x_data_1)
    return y_predictions_1

def find_locations(data, value):
    data_df=pd.DataFrame(data)
    df_locations=array([])
    df_locations=np.where(data_df==value)
    df_locations=df_locations[0].tolist()
    return df_locations
 

def populate_dataFrame(old_data_frame, locations):
    new_data_frame=pd.DataFrame()  
    new_data_frame=old_data_frame.ix[locations]
    new_data_frame=new_data_frame.iloc[:,ftrs_function].values
    return new_data_frame

def model_wnids(data):
    y_predictions_1=pd.DataFrame()####output from the first model
    y_predictions_2=pd.DataFrame()####output from the second model
    y_predictions_final=pd.DataFrame()####Final output for the embebed model
    x_data_1=pd.DataFrame(data)###input for the first model
    x_data_2=pd.DataFrame()###input for the second model
    locations=pd.DataFrame()####location for the data that goes to the second model
    y_predictions_1=model1.predict(x_data_1)
    print(y_predictions_2)
    y_predictions_1[y_predictions_1 == 1] = 2
    y_predictions_1[y_predictions_1 == 0] = 1
    y_predictions_1[y_predictions_1 == 3] = 0


    locations=find_locations(y_predictions_1, 2)
    x_data_2= populate_dataFrame(x_data_1,locations)
    y_predictions_2=model2.predict(x_data_2)
    print(y_predictions_2)
    #####Joining the predictions#########################3
    y_predictions_final=np.where(y_predictions_1==0,3,0)#####
    k1=0
    for m in locations:
        y_predictions_final[m]=y_predictions_2[k1]
        k1=k1+1
    return y_predictions_final

############for Testing purpose######################
#y_predictions_1=pd.DataFrame()####output from the first model
#y_predictions_2=pd.DataFrame()####output from the second model
#y_predictions_final=pd.DataFrame()####Final output for the embebed model
#x_data_1=pd.DataFrame(X_tst)###input for the first model
#x_data_2=pd.DataFrame()###input for the second model
#locations=pd.DataFrame()####location for the data that goes to the second model
#y_predictions_1=model1.predict(x_data_1)
#
#accu=accuracy_score(y_test_df,y_predictions_1)*100
#accu
#cm = confusion_matrix(y_test_df,y_predictions_1)
#cm
#print(cm)
#y_test_df[y_test_df != 3] = 1
#y_test_df[y_test_df == 3] = 0
#y_predictions_1[y_predictions_1 != 3] = 1
#y_predictions_1[y_predictions_1 == 3] = 0
#accu2=accuracy_score(y_test_df,y_predictions_1)*100
#accu2
#cm_final2 = confusion_matrix(y_test_df,y_predictions_1)
#cm_final2
#
#locations=find_locations(y_predictions_1, 1)
#x_data_2= populate_dataFrame(x_data_1,locations)
#y_predictions_2=model2.predict(x_data_2)
#y_predictions_final=np.where(y_predictions_1==0,3,3)##########################################revisar esta parte
##prediction_final=array([])
#k1=0
#for m in locations:
#    y_predictions_final[m]=y_predictions_2[k1]
#    k1=k1+1
#accuracy_final = accuracy_score(y_attack_test_df,y_predictions_final )*100
#cm4 = confusion_matrix(y_attack_test_df,y_predictions_final)
#print(accuracy_final)
#cm4
######################################################################

#######################################FINAL TESTING#############
y_final_predicciones=pd.DataFrame()
start_WNIDS = time.time()
y_final_predicciones=model_wnids(X_test_df)
end_WNIDS = time.time()
############RESULTS AND PLOTTING#############
accuracy_final = accuracy_score(y_attack_test_df, y_final_predicciones)*100
cm4 = confusion_matrix(y_attack_test_df,y_final_predicciones)
cm4

fig = plt.figure()
plt.imshow(cm4, interpolation='nearest', cmap=plt.cm.Blues)
plt.colorbar()
tick_marks = np.arange(len(classes4))
plt.xticks(tick_marks, classes4, rotation=45)
plt.yticks(tick_marks, classes4)

fmt = 'd'
np.set_printoptions(precision=2)
thresh = cm4.max() / 2.
for i, j in itertools.product(range(cm4.shape[0]), range(cm4.shape[1])):
    plt.text(j, i, format(cm4[i, j], fmt), horizontalalignment="center", color="white" if cm4[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label', fontweight='bold')
    plt.xlabel('Predicted label', fontweight='bold')
   
plt.show()
fig.savefig('confusion_matrix_4.png')
cr3 = classification_report(y_attack_test_df, y_final_predicciones, target_names=classes4)
print(cr3)
print("The accuracy of the WNIDS is:",accuracy_final,"%")
print('Time elapsed for Prediction with WNIDS: %.4f'%(end_WNIDS-start_WNIDS))
