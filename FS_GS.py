import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import numpy as np
import itertools
import matplotlib.pyplot as plt
from scipy.stats import mode
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from numpy import array
from sklearn.ensemble import ExtraTreesClassifier


import time

################################################DATA LOADING########################################################################
#actual dataframe
#training dataframe
srcTrn = 'datasets/Trn_NZVAR_37.csv'
dfTrn = pd.read_csv(srcTrn, header=None, low_memory= False)
#test dataframe
srcTst = 'datasets/Tst_NZVAR_37.csv'
dfTst = pd.read_csv(srcTst, header=None, low_memory= False)


#dataframe for the first model
dfTrn1=pd.DataFrame(dfTrn)
dfTst1=pd.DataFrame(dfTst)

y_actualname=dfTrn.iloc[:,-1]

################################################DATA PREPROCESSING#############################################################

#######################Feature selection - Correlation analysis based###################################
# 0, 3 epoch time
# 1, 2
# 4, 5
# 6, 7, 8, 9, 10, 11, 12
# 13 mac time
# 16 and 17 are negatively correlated
# Attributes with missing values: 15, 16, 18, 28, 29-33, 34, 35
# Att20+ Att21 = Att19

values_to_replace = {'0x00': 0, '0x01': 1, '0x02': 2}
dfTrn1[22] = dfTrn[22].map(values_to_replace)
dfTst1[22] = dfTst[22].map(values_to_replace)
#actual name of the labels
values_to_replace1 = {'normal':3, 'injection':2, 'flooding': 0, 'impersonation':1}
dfTrn1[36] = dfTrn1[36].map(values_to_replace1)
dfTst1[36] = dfTst1[36].map(values_to_replace1)


################Not Sure#################3
#######1 2 3 4 5 6 7 8 9 10 11 12  14  17 20 21 22
#######23 24 25 26 27 35 36
#ind = dfTrn1[1].isna()
#dfTrn1.at[ind, 1] = 0.0
#ind = dfTst1[1].isna()
#dfTst1.at[ind, 1] = 0.0
#
#ind = dfTrn1[2].isna()
#dfTrn1.at[ind, 2] = 0.0
#ind = dfTst1[2].isna()
#dfTst1.at[ind, 2] = 0.0
#
#ind = dfTrn1[3].isna()
#dfTrn1.at[ind, 3] = 0.0
#ind = dfTst1[3].isna()
#dfTst1.at[ind, 3] = 0.0
#
#ind = dfTrn1[4].isna()
#dfTrn1.at[ind, 4] = 0.0
#ind = dfTst1[4].isna()
#dfTst1.at[ind, 4] = 0.0
#
#ind = dfTrn1[5].isna()
#dfTrn1.at[ind, 5] = 0.0
#ind = dfTst1[5].isna()
#dfTst1.at[ind, 5] = 0.0
#
#ind = dfTrn1[6].isna()
#dfTrn1.at[ind, 6] = 0.0
#ind = dfTst1[6].isna()
#dfTst1.at[ind, 6] = 0.0
#
#ind = dfTrn1[7].isna()
#dfTrn1.at[ind, 7] = 0.0
#ind = dfTst1[7].isna()
#dfTst1.at[ind, 7] = 0.0
#
#ind = dfTrn1[8].isna()
#dfTrn1.at[ind, 8] = 0.0
#ind = dfTst1[8].isna()
#dfTst1.at[ind, 8] = 0.0
#
#ind = dfTrn1[9].isna()
#dfTrn1.at[ind, 9] = 0.0
#ind = dfTst1[9].isna()
#dfTst1.at[ind, 9] = 0.0
#
#ind = dfTrn1[10].isna()
#dfTrn1.at[ind, 10] = 0.0
#ind = dfTst1[10].isna()
#dfTst1.at[ind, 10] = 0.0
#
#ind = dfTrn1[11].isna()
#dfTrn1.at[ind, 11] = 0.0
#ind = dfTst1[11].isna()
#dfTst1.at[ind, 11] = 0.0
#
#ind = dfTrn1[12].isna()
#dfTrn1.at[ind, 12] = 0.0
#ind = dfTst1[12].isna()
#dfTst1.at[ind, 12] = 0.0
#
#ind = dfTrn1[14].isna()
#dfTrn1.at[ind, 14] = 0.0
#ind = dfTst1[14].isna()
#dfTst1.at[ind, 14] = 0.0
#
#ind = dfTrn1[17].isna()
#dfTrn1.at[ind, 17] = 0.0
#ind = dfTst1[17].isna()
#dfTst1.at[ind, 17] = 0.0
#
#ind = dfTrn1[20].isna()
#dfTrn1.at[ind, 20] = 0.0
#ind = dfTst1[20].isna()
#dfTst1.at[ind, 20] = 0.0
#
#ind = dfTrn1[21].isna()
#dfTrn1.at[ind, 21] = 0.0
#ind = dfTst1[21].isna()
#dfTst1.at[ind, 21] = 0.0
#
#ind = dfTrn1[22].isna()
#dfTrn1.at[ind, 22] = 0.0
#ind = dfTst1[22].isna()
#dfTst1.at[ind, 22] = 0.0
#
#ind = dfTrn1[23].isna()
#dfTrn1.at[ind, 23] = 0.0
#ind = dfTst1[23].isna()
#dfTst1.at[ind, 23] = 0.0
#
#ind = dfTrn1[24].isna()
#dfTrn1.at[ind, 24] = 0.0
#ind = dfTst1[24].isna()
#dfTst1.at[ind, 24] = 0.0
#
#ind = dfTrn1[25].isna()
#dfTrn1.at[ind, 25] = 0.0
#ind = dfTst1[25].isna()
#dfTst1.at[ind, 25] = 0.0
#
#ind = dfTrn1[26].isna()
#dfTrn1.at[ind, 26] = 0.0
#ind = dfTst1[26].isna()
#dfTst1.at[ind, 26] = 0.0
#
#ind = dfTrn1[27].isna()
#dfTrn1.at[ind, 27] = 0.0
#ind = dfTst1[27].isna()
#dfTst1.at[ind, 27] = 0.0
#
#ind = dfTrn1[35].isna()
#dfTrn1.at[ind, 35] = 0.0
#ind = dfTst1[35].isna()
#dfTst1.at[ind, 35] = 0.0
#
#ind = dfTrn1[36].isna()
#dfTrn1.at[ind, 36] = 0.0
#ind = dfTst1[36].isna()
#dfTst1.at[ind, 36] = 0.0
##########################


ind = dfTrn1[15].isna()
dfTrn1.at[ind, 15] = 2437.0
ind = dfTst1[15].isna()
dfTst1.at[ind, 15] = 2437.0

ind = dfTrn1[16].isna()
dfTrn1.at[ind, 16] = 0.0
ind = dfTst1[16].isna()
dfTst1.at[ind, 16] = 0.0

ind = dfTrn1[18].isna()
dfTrn1.at[ind, 18] = -21.0
ind = dfTst1[18].isna()
dfTst1.at[ind, 18] = -21.0

ind = dfTrn1[28].isna()
dfTrn1.at[ind, 28] = 44.0
ind = dfTst1[28].isna()
dfTst1.at[ind, 28] = 44.0

ind = dfTrn1[29].notna()
dfTrn1.at[ind, 29] = 1
ind = dfTrn1[29].isna()
dfTrn1.at[ind, 29] = 0

ind = dfTst1[29].notna()
dfTst1.at[ind, 29] = 1
ind = dfTst1[29].isna()
dfTst1.at[ind, 29] = 0

ind = dfTrn1[30].notna()
dfTrn1.at[ind, 30] = 1
ind = dfTrn1[30].isna()
dfTrn1.at[ind, 30] = 0

ind = dfTst1[30].notna()
dfTst1.at[ind, 30] = 1
ind = dfTst1[30].isna()
dfTst1.at[ind, 30] = 0

ind = dfTrn1[31].notna()
dfTrn1.at[ind, 31] = 1
ind = dfTrn1[31].isna()
dfTrn1.at[ind, 31] = 0

ind = dfTst1[31].notna()
dfTst1.at[ind, 31] = 1
ind = dfTst1[31].isna()
dfTst1.at[ind, 31] = 0

ind = dfTrn1[32].notna()
dfTrn1.at[ind, 32] = 1
ind = dfTrn1[32].isna()
dfTrn1.at[ind, 32] = 0

ind = dfTst1[32].notna()
dfTst1.at[ind, 32] = 1
ind = dfTst1[32].isna()
dfTst1.at[ind, 32] = 0

ind = dfTrn1[33].notna()
dfTrn1.at[ind, 33] = 1
ind = dfTrn1[33].isna()
dfTrn1.at[ind, 33] = 0

ind = dfTst1[33].notna()
dfTst1.at[ind, 33] = 1
ind = dfTst1[33].isna()
dfTst1.at[ind, 33] = 0

ind = dfTrn1[34].isna()
dfTrn1.at[ind, 34] = 0
ind = dfTst1[34].isna()
dfTst1.at[ind, 34] = 0

########################Features selected after FEATURE IMPORTANCE ANALYSIS############################
###96.4359% random forest firsst model 4 classes
#ftrs_lst = list(range(1,13))+list(range(14,19))+list(range(20,37))
#ftrs_lst = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34]
#ftrs_lst = [4, 14, 18, 20, 21, 22, 24, 25, 27, 28]
#ftrs_lst = [4, 14, 16, 18, 20, 21, 22, 25, 27, 28]
###95.8734% the 4 classes one model
#ftrs_lst = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34]

##ftrs_lst = [20,16,27,14,22,25,26,4,18,21]
#########FEATURES SELECTED####
########ACCURACY OF 98.803%
ftrs_lst = [4, 14, 16, 18, 20, 21, 22, 25,26, 27]
#########FEATURES SELECTED####
########ACCURACY OF 95.873%
#ftrs_lst = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34]

#########FEATURES SELECTED#### (Best 15 using feature importance after correlation analysis)
#ftrs_lst = [4, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30]



##Features for the first model prediction - INPUTS for the first MODEL  (training)
X = dfTrn1.iloc[:, ftrs_lst].values
##Features for the first model prediction - Outputs for the first MODEL (training)
Y = dfTrn1.iloc[:, 36].values
##Features for the first model prediction - INPUTS for the first MODEL  (test)
X_tst = dfTst1.iloc[:, ftrs_lst].values
##Features for the first model prediction - Outputs for the first MODEL (test)
Y_tst = dfTst1.iloc[:, 36].values


##splitting the data in training and test sets
##Converting the data from array to dataframe to keep track of them
#########ORIGINAL DATA FOR REFERENCES########################
X_train_df=pd.DataFrame(X)
X_test_df=pd.DataFrame(X_tst)
y_train_df=pd.DataFrame(Y)
y_test_df=pd.DataFrame(Y_tst)


############DATA FRAME for output USING FOR references after the first classifier
y_train_df1=pd.DataFrame()
y_train_df1=y_train_df1.append(y_train_df)

y_test_df1=pd.DataFrame()
y_test_df1=y_test_df1.append(y_test_df)

########GRIDSEARCHCV#############################
from sklearn.model_selection import GridSearchCV
rs=list(range(10))
##gs=GridSearchCV(RandomForestClassifier(), {"random_state":[rs]}, n_jobs=-1, cv=5, scoring="accuracy")
gs=GridSearchCV(RandomForestClassifier(), { 'n_estimators'      : [320,330,340],
                                            'max_depth'         : [8, 9, 10, 11, 12],
                                            'random_state'      : [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]}, n_jobs=-1, cv=5, scoring="accuracy")
gs.fit(X_train_df,y_train_df)
print(gs.best_params_)
print(gs.best_score_)
model1=RandomForestClassifier.set_params(**gs.best_params_)
model1.fit(X_train_df,y_train_df)
predictions1=model1.predict(X_test_df)
accuracy_model1=accuracy_score(y_test_df,predictions1)
#######FEATURE SELECTION#########
from sklearn.feature_selection import RFE
#################################################RECURSIVE FEATURE ELIMINATION###########################
model=RandomForestClassifier(random_state = 0)
rfe=RFE(model, 6)
rfe=rfe.fit(X_train_df,y_train_df);
print(rfe.support_)
print(rfe.ranking_)
#################################################FEATURE IMPORTANCE#####################################
model.fit(X_train_df,y_train_df)
print(model.feature_importances_)
feat_importances = pd.Series(model.feature_importances_, index=X_test_df.columns)
feat_importances.nlargest(10).plot(kind='barh')
plt.show()

#########################creating an explainer########
from lime.lime_tabular import limeTabularExplainer