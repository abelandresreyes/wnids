import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import numpy as np
import itertools
import matplotlib.pyplot as plt
from scipy.stats import mode
from sklearn.model_selection import cross_val_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from numpy import array
from sklearn.ensemble import ExtraTreesClassifier


import time

################################################DATA LOADING########################################################################
#actual dataframe
#training dataframe
srcTrn = 'datasets/Trn_NZVAR_37.csv'
dfTrn = pd.read_csv(srcTrn, header=None, low_memory= False)
#test dataframe
srcTst = 'datasets/Tst_NZVAR_37.csv'
dfTst = pd.read_csv(srcTst, header=None, low_memory= False)


#dataframe for the first model
dfTrn1=pd.DataFrame(dfTrn)
dfTst1=pd.DataFrame(dfTst)

y_actualname=dfTrn.iloc[:,-1]

################################################DATA PREPROCESSING#############################################################

#######################Feature selection - Correlation analysis based###################################
# 0, 3 epoch time
# 1, 2
# 4, 5
# 6, 7, 8, 9, 10, 11, 12
# 13 mac time
# 16 and 17 are negatively correlated
# Attributes with missing values: 15, 16, 18, 28, 29-33, 34, 35
# Att20+ Att21 = Att19

values_to_replace = {'0x00': 0, '0x01': 1, '0x02': 2}
dfTrn1[22] = dfTrn[22].map(values_to_replace)
dfTst1[22] = dfTst[22].map(values_to_replace)
#actual name of the labels
values_to_replace1 = {'normal':3, 'injection':2, 'flooding': 0, 'impersonation':1}
dfTrn1[36] = dfTrn1[36].map(values_to_replace1)
dfTst1[36] = dfTst1[36].map(values_to_replace1)


################Not Sure#################3
#######1 2 3 4 5 6 7 8 9 10 11 12  14  17 20 21 22
#######23 24 25 26 27 35 36
#ind = dfTrn1[1].isna()
#dfTrn1.at[ind, 1] = 0.0
#ind = dfTst1[1].isna()
#dfTst1.at[ind, 1] = 0.0
#
#ind = dfTrn1[2].isna()
#dfTrn1.at[ind, 2] = 0.0
#ind = dfTst1[2].isna()
#dfTst1.at[ind, 2] = 0.0
#
#ind = dfTrn1[3].isna()
#dfTrn1.at[ind, 3] = 0.0
#ind = dfTst1[3].isna()
#dfTst1.at[ind, 3] = 0.0
#
#ind = dfTrn1[4].isna()
#dfTrn1.at[ind, 4] = 0.0
#ind = dfTst1[4].isna()
#dfTst1.at[ind, 4] = 0.0
#
#ind = dfTrn1[5].isna()
#dfTrn1.at[ind, 5] = 0.0
#ind = dfTst1[5].isna()
#dfTst1.at[ind, 5] = 0.0
#
#ind = dfTrn1[6].isna()
#dfTrn1.at[ind, 6] = 0.0
#ind = dfTst1[6].isna()
#dfTst1.at[ind, 6] = 0.0
#
#ind = dfTrn1[7].isna()
#dfTrn1.at[ind, 7] = 0.0
#ind = dfTst1[7].isna()
#dfTst1.at[ind, 7] = 0.0
#
#ind = dfTrn1[8].isna()
#dfTrn1.at[ind, 8] = 0.0
#ind = dfTst1[8].isna()
#dfTst1.at[ind, 8] = 0.0
#
#ind = dfTrn1[9].isna()
#dfTrn1.at[ind, 9] = 0.0
#ind = dfTst1[9].isna()
#dfTst1.at[ind, 9] = 0.0
#
#ind = dfTrn1[10].isna()
#dfTrn1.at[ind, 10] = 0.0
#ind = dfTst1[10].isna()
#dfTst1.at[ind, 10] = 0.0
#
#ind = dfTrn1[11].isna()
#dfTrn1.at[ind, 11] = 0.0
#ind = dfTst1[11].isna()
#dfTst1.at[ind, 11] = 0.0
#
#ind = dfTrn1[12].isna()
#dfTrn1.at[ind, 12] = 0.0
#ind = dfTst1[12].isna()
#dfTst1.at[ind, 12] = 0.0
#
#ind = dfTrn1[14].isna()
#dfTrn1.at[ind, 14] = 0.0
#ind = dfTst1[14].isna()
#dfTst1.at[ind, 14] = 0.0
#
#ind = dfTrn1[17].isna()
#dfTrn1.at[ind, 17] = 0.0
#ind = dfTst1[17].isna()
#dfTst1.at[ind, 17] = 0.0
#
#ind = dfTrn1[20].isna()
#dfTrn1.at[ind, 20] = 0.0
#ind = dfTst1[20].isna()
#dfTst1.at[ind, 20] = 0.0
#
#ind = dfTrn1[21].isna()
#dfTrn1.at[ind, 21] = 0.0
#ind = dfTst1[21].isna()
#dfTst1.at[ind, 21] = 0.0
#
#ind = dfTrn1[22].isna()
#dfTrn1.at[ind, 22] = 0.0
#ind = dfTst1[22].isna()
#dfTst1.at[ind, 22] = 0.0
#
#ind = dfTrn1[23].isna()
#dfTrn1.at[ind, 23] = 0.0
#ind = dfTst1[23].isna()
#dfTst1.at[ind, 23] = 0.0
#
#ind = dfTrn1[24].isna()
#dfTrn1.at[ind, 24] = 0.0
#ind = dfTst1[24].isna()
#dfTst1.at[ind, 24] = 0.0
#
#ind = dfTrn1[25].isna()
#dfTrn1.at[ind, 25] = 0.0
#ind = dfTst1[25].isna()
#dfTst1.at[ind, 25] = 0.0
#
#ind = dfTrn1[26].isna()
#dfTrn1.at[ind, 26] = 0.0
#ind = dfTst1[26].isna()
#dfTst1.at[ind, 26] = 0.0
#
#ind = dfTrn1[27].isna()
#dfTrn1.at[ind, 27] = 0.0
#ind = dfTst1[27].isna()
#dfTst1.at[ind, 27] = 0.0
#
#ind = dfTrn1[35].isna()
#dfTrn1.at[ind, 35] = 0.0
#ind = dfTst1[35].isna()
#dfTst1.at[ind, 35] = 0.0
#
#ind = dfTrn1[36].isna()
#dfTrn1.at[ind, 36] = 0.0
#ind = dfTst1[36].isna()
#dfTst1.at[ind, 36] = 0.0
##########################


ind = dfTrn1[15].isna()
dfTrn1.at[ind, 15] = 2437.0
ind = dfTst1[15].isna()
dfTst1.at[ind, 15] = 2437.0

ind = dfTrn1[16].isna()
dfTrn1.at[ind, 16] = 0.0
ind = dfTst1[16].isna()
dfTst1.at[ind, 16] = 0.0

ind = dfTrn1[18].isna()
dfTrn1.at[ind, 18] = -21.0
ind = dfTst1[18].isna()
dfTst1.at[ind, 18] = -21.0

ind = dfTrn1[28].isna()
dfTrn1.at[ind, 28] = 44.0
ind = dfTst1[28].isna()
dfTst1.at[ind, 28] = 44.0

ind = dfTrn1[29].notna()
dfTrn1.at[ind, 29] = 1
ind = dfTrn1[29].isna()
dfTrn1.at[ind, 29] = 0

ind = dfTst1[29].notna()
dfTst1.at[ind, 29] = 1
ind = dfTst1[29].isna()
dfTst1.at[ind, 29] = 0

ind = dfTrn1[30].notna()
dfTrn1.at[ind, 30] = 1
ind = dfTrn1[30].isna()
dfTrn1.at[ind, 30] = 0

ind = dfTst1[30].notna()
dfTst1.at[ind, 30] = 1
ind = dfTst1[30].isna()
dfTst1.at[ind, 30] = 0

ind = dfTrn1[31].notna()
dfTrn1.at[ind, 31] = 1
ind = dfTrn1[31].isna()
dfTrn1.at[ind, 31] = 0

ind = dfTst1[31].notna()
dfTst1.at[ind, 31] = 1
ind = dfTst1[31].isna()
dfTst1.at[ind, 31] = 0

ind = dfTrn1[32].notna()
dfTrn1.at[ind, 32] = 1
ind = dfTrn1[32].isna()
dfTrn1.at[ind, 32] = 0

ind = dfTst1[32].notna()
dfTst1.at[ind, 32] = 1
ind = dfTst1[32].isna()
dfTst1.at[ind, 32] = 0

ind = dfTrn1[33].notna()
dfTrn1.at[ind, 33] = 1
ind = dfTrn1[33].isna()
dfTrn1.at[ind, 33] = 0

ind = dfTst1[33].notna()
dfTst1.at[ind, 33] = 1
ind = dfTst1[33].isna()
dfTst1.at[ind, 33] = 0

ind = dfTrn1[34].isna()
dfTrn1.at[ind, 34] = 0
ind = dfTst1[34].isna()
dfTst1.at[ind, 34] = 0

########################Features selected after FEATURE IMPORTANCE ANALYSIS############################
###96.4359% random forest firsst model 4 classes
#ftrs_lst = list(range(1,13))+list(range(14,19))+list(range(20,37))
#ftrs_lst = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34]
#ftrs_lst = [4, 14, 18, 20, 21, 22, 24, 25, 27, 28]
#ftrs_lst = [4, 14, 16, 18, 20, 21, 22, 25, 27, 28]
###95.8734% the 4 classes one model
#ftrs_lst = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34]

##ftrs_lst = [20,16,27,14,22,25,26,4,18,21]
#########FEATURES SELECTED####
########ACCURACY OF 98.803%
ftrs_lst = [4, 14, 16, 18, 20, 21, 22, 25,26, 27]
#########FEATURES SELECTED####
########ACCURACY OF 95.873%
#ftrs_lst = [4, 8, 14, 15, 16, 18, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 34]

#########FEATURES SELECTED#### (Best 15 using feature importance after correlation analysis)
#ftrs_lst = [4, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30]



##Features for the first model prediction - INPUTS for the first MODEL  (training) 
X = dfTrn1.iloc[:, ftrs_lst].values
##Features for the first model prediction - Outputs for the first MODEL (training)
Y = dfTrn1.iloc[:, 36].values
##Features for the first model prediction - INPUTS for the first MODEL  (test)
X_tst = dfTst1.iloc[:, ftrs_lst].values
##Features for the first model prediction - Outputs for the first MODEL (test)
Y_tst = dfTst1.iloc[:, 36].values


##splitting the data in training and test sets
##Converting the data from array to dataframe to keep track of them
#########ORIGINAL DATA FOR REFERENCES########################
X_train_df=pd.DataFrame(X)
X_test_df=pd.DataFrame(X_tst)
y_train_df=pd.DataFrame(Y)
y_test_df=pd.DataFrame(Y_tst)


############DATA FRAME for output USING FOR references after the first classifier 
y_train_df1=pd.DataFrame()
y_train_df1=y_train_df1.append(y_train_df)

y_test_df1=pd.DataFrame()
y_test_df1=y_test_df1.append(y_test_df)



##############################################################FIRST CLASSIFIER###############################################

###several predictions usign RANDOM FOREST CLASSIFIER with different states
m1 = RandomForestClassifier(random_state = 0)
m1.fit(X_train_df,y_train_df)
p1 = m1.predict(X_test_df)
p1 = p1.reshape(p1.shape[0], 1)

m2 = RandomForestClassifier(random_state = 7)
m2.fit(X_train_df,y_train_df)
p2 = m2.predict(X_test_df)
p2 = p2.reshape(p2.shape[0], 1)

m3 = RandomForestClassifier(random_state = 14)
m3.fit(X_train_df,y_train_df)
p3 = m3.predict(X_test_df)
p3 = p3.reshape(p3.shape[0], 1)

m4 = RandomForestClassifier(random_state = 21)
m4.fit(X_train_df,y_train_df)
p4 = m4.predict(X_test_df)
p4 = p4.reshape(p4.shape[0], 1)

m5 = RandomForestClassifier(random_state = 28)
m5.fit(X_train_df,y_train_df)
p5 = m5.predict(X_test_df)
p5 = p5.reshape(p5.shape[0], 1)

m6 = RandomForestClassifier(random_state = 35)
m6.fit(X_train_df,y_train_df)
p6 = m6.predict(X_test_df)
p6 = p6.reshape(p6.shape[0], 1)

m7 = RandomForestClassifier(random_state = 42)
m7.fit(X_train_df,y_train_df)
p7 = m7.predict(X_test_df)
p7 = p7.reshape(p7.shape[0], 1)

m8 = RandomForestClassifier(random_state = 49)
m8.fit(X_train_df,y_train_df)
p8 = m8.predict(X_test_df)
p8 = p8.reshape(p8.shape[0], 1)

m9 = RandomForestClassifier(random_state = 56)
m9.fit(X_train_df,y_train_df)
p9 = m9.predict(X_test_df)
p9 = p9.reshape(p9.shape[0], 1)

m10 = RandomForestClassifier(random_state = 63)
m10.fit(X_train_df,y_train_df)
p10 = m10.predict(X_test_df)
p10 = p10.reshape(p10.shape[0], 1)

m11 = RandomForestClassifier(random_state = 70)
m11.fit(X_train_df,y_train_df)
p11 = m11.predict(X_test_df)
p11 = p11.reshape(p11.shape[0], 1)

m12 = RandomForestClassifier(random_state = 77)
m12.fit(X_train_df,y_train_df)
p12 = m12.predict(X_test_df)
p12 = p12.reshape(p12.shape[0], 1)

m13 = RandomForestClassifier(random_state = 84)
m13.fit(X_train_df,y_train_df)
p13 = m13.predict(X_test_df)
p13 = p13.reshape(p13.shape[0], 1)

m14 = RandomForestClassifier(random_state = 91)
m14.fit(X_train_df,y_train_df)
p14 = m14.predict(X_test_df)
p14 = p14.reshape(p14.shape[0], 1)

m15 = RandomForestClassifier(random_state = 98)
m15.fit(X_train_df,y_train_df)
p15 = m15.predict(X_test_df)
p15 = p15.reshape(p15.shape[0], 1)

m16 = RandomForestClassifier(random_state = 105)
m16.fit(X_train_df,y_train_df)
p16 = m16.predict(X_test_df)
p16 = p16.reshape(p16.shape[0], 1)

m17 = RandomForestClassifier(random_state = 112)
m17.fit(X_train_df,y_train_df)
p17 = m17.predict(X_test_df)
p17 = p17.reshape(p17.shape[0], 1)

m18 = RandomForestClassifier(random_state = 119)
m18.fit(X_train_df,y_train_df)
p18 = m18.predict(X_test_df)
p18 = p18.reshape(p18.shape[0], 1)

m19 = RandomForestClassifier(random_state = 126)
m19.fit(X_train_df,y_train_df)
p19 = m19.predict(X_test_df)
p19 = p19.reshape(p19.shape[0], 1)

m20 = RandomForestClassifier(random_state = 133)
m20.fit(X_train_df,y_train_df)
p20 = m20.predict(X_test_df)
p20 = p20.reshape(p20.shape[0], 1)

m21 = RandomForestClassifier(random_state = 140)
m21.fit(X_train_df,y_train_df)
p21 = m21.predict(X_test_df)
p21 = p21.reshape(p21.shape[0], 1)

m22 = RandomForestClassifier(random_state = 147)
m22.fit(X_train_df,y_train_df)
p22 = m22.predict(X_test_df)
p22 = p22.reshape(p22.shape[0], 1)

m23 = RandomForestClassifier(random_state = 154)
m23.fit(X_train_df,y_train_df)
p23 = m23.predict(X_test_df)
p23 = p23.reshape(p23.shape[0], 1)

m24 = RandomForestClassifier(random_state = 161)
m24.fit(X_train_df,y_train_df)
p24 = m24.predict(X_test_df)
p24 = p24.reshape(p24.shape[0], 1)

m25 = RandomForestClassifier(random_state = 168)
m25.fit(X_train_df,y_train_df)
p25 = m25.predict(X_test_df)
p25 = p25.reshape(p25.shape[0], 1)

m26 = RandomForestClassifier(random_state = 175)
m26.fit(X_train_df,y_train_df)
p26 = m26.predict(X_test_df)
p26 = p26.reshape(p26.shape[0], 1)

m27 = RandomForestClassifier(random_state = 182)
m27.fit(X_train_df,y_train_df)
p27 = m27.predict(X_test_df)
p27 = p27.reshape(p27.shape[0], 1)

m28 = RandomForestClassifier(random_state = 189)
m28.fit(X_train_df,y_train_df)
p28 = m28.predict(X_test_df)
p28 = p28.reshape(p28.shape[0], 1)

m29 = RandomForestClassifier(random_state = 196)
m29.fit(X_train_df,y_train_df)
p29 = m29.predict(X_test_df)
p29 = p29.reshape(p29.shape[0], 1)

m30 = RandomForestClassifier(random_state = 203)
m30.fit(X_train_df,y_train_df)
p30 = m30.predict(X_test_df)
p30 = p30.reshape(p30.shape[0], 1)

m31 = RandomForestClassifier(random_state = 210)
m31.fit(X_train_df,y_train_df)
p31 = m31.predict(X_test_df)
p31 = p31.reshape(p31.shape[0], 1)


p = np.concatenate((p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15,
                    p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27,
                    p28, p29, p30, p31), axis=1)



##########################################Predictions of the first model
start = time.time()
predictions = mode(p, axis=1)[0]
end = time.time()
accuracy_1 = accuracy_score(y_test_df1, predictions)*100
print("The accuray after the first classifier (4 classes)is: ")
print(accuracy_1)
####98.80% 4 classes
cm1=confusion_matrix(y_test_df1,predictions)
cm1


classes4 = ['flooding', 'impersonation', 'injection', 'normal']
fig = plt.figure()
plt.imshow(cm1, interpolation='nearest', cmap=plt.cm.Blues)
plt.colorbar()
tick_marks = np.arange(len(classes4))
plt.xticks(tick_marks, classes4, rotation=45)
plt.yticks(tick_marks, classes4)

fmt = 'd'
np.set_printoptions(precision=2)
thresh = cm1.max() / 2.
for i, j in itertools.product(range(cm1.shape[0]), range(cm1.shape[1])):
    plt.text(j, i, format(cm1[i, j], fmt), horizontalalignment="center", color="white" if cm1[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label', fontweight='bold')
    plt.xlabel('Predicted label', fontweight='bold')
    
plt.show()
fig.savefig('confusion_matrix_1.png')

cr1 = classification_report(y_test_df1, predictions, target_names=classes4)
print(cr1)



##Two classes classification based on the output from the first model
values_to_replace_two_clases = {3:0, 1:1, 2:1, 0:1}
y_train_df1[0]=y_train_df1[0].map(values_to_replace_two_clases)
y_test_df1[y_test_df1 != 3] = 1 #this is to rename create a new class that enclose all attacks
y_test_df1[y_test_df1 == 3] = 0 #the normal attack will remains as a normal class
predictions[predictions != 3] = 1
predictions[predictions == 3] = 0
accuracy_2 = accuracy_score(y_test_df1, predictions)*100
print("The accuray after the first classifier (2 classes)is: ")
print(accuracy_2)
cm2 = confusion_matrix(y_test_df1, predictions)

classes2 = ['normal', 'attack']
cr2 = classification_report(y_test_df1, predictions, target_names=classes2)
print(cr2)

fig = plt.figure()
plt.imshow(cm2, interpolation='nearest', cmap=plt.cm.Blues)
plt.colorbar()
tick_marks = np.arange(len(classes2))
plt.xticks(tick_marks, classes2, rotation=0)
plt.yticks(tick_marks, classes2)

fmt = 'd'
np.set_printoptions(precision=2)
thresh = cm2.max() / 2.
for i, j in itertools.product(range(cm2.shape[0]), range(cm2.shape[1])):
    plt.text(j, i, format(cm2[i, j], fmt), horizontalalignment="center", color="white" if cm2[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label', fontweight='bold')
    plt.xlabel('Predicted label', fontweight='bold')
    
plt.show()
fig.savefig('confusion_matrix_2.png')



################################################################DATA PREPROCESING FOR THE SECOND CLASSIFIER#####################
#The idea is to keep track of the dataframes that were classified as ATTACK (it could be either flooding, injection or impersonation) to use those
#data frames as input for the second classifier in order to classified them in their correspondent attack frame.

###########DATA FRAME WITH JUST ATTACKS FRAMES#############################3
#The goal is to create a data training set with just attack in order to create a classifier model for that, this model will classifie
# if the data frame is either flooding, impersonation or injection
##Locations from attacks frames
df_locations_row=array([])
df_locations_row=np.where(y_train_df1==1)
df_locations_row=df_locations_row[0].tolist()


df2=pd.DataFrame()
##populating the second dataframe
for j in range (len(df_locations_row)):
#for j in range (3):
    row_index=df_locations_row[j]
    temp=pd.DataFrame()
    temp=X_train_df.ix[row_index]
    df2=df2.append(temp)



##implementing the second model

X_2=df2.iloc[:,:]
#create Y_2 de tal manera que tenga las ouputs originales de las rows que fueron identificadas despues del modelo1
y_2=pd.DataFrame()
for j in range (len(df_locations_row)):
    row_index=df_locations_row[j]
    temp=pd.DataFrame()
    temp=y_train_df.ix[row_index]
    y_2=y_2.append(temp)




df_locations_row_test=array([])
df_locations_row_test=np.where(y_test_df1==1)
df_locations_row_test=df_locations_row_test[0].tolist()


df2_test=pd.DataFrame()
##populating the second dataframe
for j in range (len(df_locations_row_test)):
#for j in range (3):
    row_index=df_locations_row_test[j]
    temp=pd.DataFrame()
    temp=X_test_df.ix[row_index]
    df2_test=df2_test.append(temp)



##implementing the second model

X_2_test=df2_test.iloc[:,:]
#create Y_2 de tal manera que tenga las ouputs originales de las rows que fueron identificadas despues del modelo1
y_2_test=pd.DataFrame()
for j in range (len(df_locations_row_test)):
    row_index=df_locations_row_test[j]
    temp=pd.DataFrame()
    temp=y_test_df.ix[row_index]
    y_2_test=y_2_test.append(temp)



seed_lst = [j*7 for j in range(31)]
mean_scores = []
start = time.time()
for seed in seed_lst:
    model = ExtraTreesClassifier(random_state = seed)
    scores = cross_val_score(model, X_2, y_2, cv=10, scoring='accuracy')
    score = scores.mean()*100
    mean_scores.append(score)  

model.fit(X_2,y_2)
y_pred=model.predict(X_2_test)


accuracy_3 = accuracy_score(y_2_test, y_pred)*100
print(accuracy_3)


cm3 = confusion_matrix(y_2_test, y_pred)



prediction_final=np.where(predictions==0,3,0)
#prediction_final=array([])
k1=0
for k in df_locations_row_test:
    prediction_final[k]=y_pred[k1]
    k1=k1+1
    #prediction_final[k]=y_pred[k]

accuracy3 = accuracy_score(y_test_df, prediction_final)*100
cm3 = confusion_matrix(y_test_df,prediction_final)
print(accuracy3)
cm3




classes = ['flooding', 'impersonation', 'injection', 'normal']
fig = plt.figure()
plt.imshow(cm3, interpolation='nearest', cmap=plt.cm.Blues)
plt.colorbar()
tick_marks = np.arange(len(classes))
plt.xticks(tick_marks, classes, rotation=45)
plt.yticks(tick_marks, classes)

fmt = 'd'
np.set_printoptions(precision=2)
thresh = cm3.max() / 2.
for i, j in itertools.product(range(cm3.shape[0]), range(cm3.shape[1])):
    plt.text(j, i, format(cm3[i, j], fmt), horizontalalignment="center", color="white" if cm3[i, j] > thresh else "black")
    plt.tight_layout()
    plt.ylabel('True label', fontweight='bold')
    plt.xlabel('Predicted label', fontweight='bold')
    
plt.show()
fig.savefig('confusion_matrix3.png')

